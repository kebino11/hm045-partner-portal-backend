const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')

const PORT = 3000

const userApi = require('./routes/user-api')
const productApi = require('./routes/product-api')
const transactionApi = require('./routes/transaction-api')
const transactionTypeApi = require('./routes/transaction-type-api')

app.use(cors())
app.use(bodyParser.json())

app.use('/api/user', userApi)
app.use('/api/product', productApi)
app.use('/api/transaction', transactionApi)
app.use('/api/transactionType', transactionTypeApi)

app.listen(PORT, () => {
    console.log(`app listening to port: ${ process.env.PORT || PORT}`)
})