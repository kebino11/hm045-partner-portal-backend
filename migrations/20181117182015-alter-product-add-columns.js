'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.addColumn(
        'products',
        'description',
        {
          type: Sequelize.STRING
        }
      ).then(() => {
        return queryInterface.addColumn(
          'products',
          'reviews',
          {
            type: Sequelize.STRING
          }
        )
      }).then(() => {
        return queryInterface.addColumn(
          'products',
          'ratings',
          {
            type: Sequelize.INTEGER
          }
        )
      }).then(() => {
        return queryInterface.addColumn(
          'products',
          'isAddedToCart',
          {
            type: Sequelize.BOOLEAN,
            defaultValue: false
          }
        )
      }).then(() => {
        return queryInterface.addColumn(
          'products',
          'isAddedBtn',
          {
            type: Sequelize.BOOLEAN,
            defaultValue: false
          }
        )
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'products',
      'description'
    ).then(() => {
      return queryInterface.removeColumn(
        'products',
        'reviews'
      )
    }).then(() => {
      return queryInterface.removeColumn(
        'products',
        'ratings'
      )
    }).then(() => {
      return queryInterface.removeColumn(
        'products',
        'isAddedToCart'
      )
    }).then(() => {
      return queryInterface.removeColumn(
        'products',
        'isAddedBtn'
      )
    })
  }
};
