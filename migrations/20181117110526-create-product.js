'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.DECIMAL
      },
      tag: {
        type: Sequelize.STRING
      },
      quantity: {
        type: Sequelize.INTEGER
      },
      discount: {
        type: Sequelize.DECIMAL
      },
      userId: {
        type: Sequelize.INTEGER
      },
      validFrom: {
        type: Sequelize.DATE
      },
      validUntil: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(() => {
      return queryInterface.addConstraint(
        'products',
        ['userId'],
        {
          type: 'foreign key',
          name: 'product_user_id',
          references: {
            table: 'users',
            field: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        }
      )
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('products');
  }
};