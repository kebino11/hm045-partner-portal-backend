'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.INTEGER
      },
      transactionTypeId: {
        type: Sequelize.INTEGER
      },
      amount: {
        type: Sequelize.DECIMAL
      },
      productId: {
        type: Sequelize.INTEGER
      },
      referenceNumber: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(() => {
      return queryInterface.addConstraint(
        'transactions',
        ['userId'],
        {
          type: 'foreign key',
          name: 'transaction_user_id',
          references: {
            table: 'users',
            field: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        }
      )
    }).then(() => {
      return queryInterface.addConstraint(
        'transactions',
        ['productId'],
        {
          type: 'foreign key',
          name: 'transaction_product_id',
          references: {
            table: 'products',
            field: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        }
      )
    }).then(() => {
      return queryInterface.addConstraint(
        'transactions',
        ['transactionTypeId'],
        {
          type: 'foreign key',
          name: 'transaction_transactiontype_id',
          references: {
            table: 'transactionTypes',
            field: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        }
      )
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('transactions');
  }
};