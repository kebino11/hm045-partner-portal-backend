'use strict';
module.exports = (sequelize, DataTypes) => {
  const product = sequelize.define('product', {
    name: DataTypes.STRING,
    price: DataTypes.DECIMAL,
    discount: DataTypes.DECIMAL,
    userId: DataTypes.INTEGER,
    tag: DataTypes.STRING,
    quantity: DataTypes.INTEGER,
    validFrom: DataTypes.DATE,
    validUntil: DataTypes.DATE,
    description: DataTypes.STRING,
    reviews: DataTypes.STRING,
    ratings: DataTypes.INTEGER,
    isAddedToCart: DataTypes.BOOLEAN,
    isAddedBtn: DataTypes.BOOLEAN
  }, {});
  product.associate = function(models) {
    product.belongsTo(models.user, {
      foreignKey: product.rawAttributes.userId
    })
  };
  return product;
};