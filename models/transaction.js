'use strict';
module.exports = (sequelize, DataTypes) => {
  const transaction = sequelize.define('transaction', {
    userId: DataTypes.INTEGER,
    transactionTypeId: DataTypes.INTEGER,
    amount: DataTypes.DECIMAL,
    productId: DataTypes.INTEGER,
    referenceNumber: DataTypes.STRING
  }, {});
  transaction.associate = function(models) {
    transaction.belongsTo(models.user, {
      foreignKey: transaction.rawAttributes.userId
    })
    transaction.belongsTo(models.product, {
      foreignKey: transaction.rawAttributes.productId
    })
    transaction.belongsTo(models.transactionType, {
      foreignKey: transaction.rawAttributes.transactionTypeId
    })
  };
  return transaction;
};