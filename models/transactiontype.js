'use strict';
module.exports = (sequelize, DataTypes) => {
  const transactionType = sequelize.define('transactionType', {
    name: DataTypes.STRING
  }, {});
  transactionType.associate = function(models) {
    // associations can be defined here
  };
  return transactionType;
};