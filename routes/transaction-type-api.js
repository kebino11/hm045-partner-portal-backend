const express = require('express')
const router = express.Router()
const models = require('../models')
const Op = require('sequelize').Op

router.get('/', (req, res) => {
    models.transactionType.findAll({})
        .then(result => res.send(result))
        .catch(err => res.send(err))
})

router.get('/:id', (req, res) => {
    models.transactionType.findById(req.params.id)
        .then(result => res.send(result))
        .catch(err => res.send(err))
})

router.post('/', (req, res) => {
    let data = req.body
    data.createdAt = new Date()
    data.updatedAt = new Date()
    models.transactionType.create(data)
        .then(result => res.sendStatus(201))
        .catch(err => res.send(err))
})

router.put('/:id', (req, res) => {
    let data = req.body
    data.updatedAt = new Date()
    models.transactionType.findById(req.params.id)
        .then(type => {
            type.update(data)
                .then(result => res.send(200))
                .catch(err => res.send(err))
        }).catch(err => res.send(err))
})

module.exports = router