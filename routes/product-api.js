const express = require('express')
const router = express.Router()
const models = require('../models')
const Op = require('sequelize').Op

router.get('/', (req, res) => {
    models.product.findAll({
        include: [
            { model: models.user }
        ]
    })
    .then(result => res.send(result))
    .catch(err => res.send(err))
})

router.get('/brand/:name', (req, res) => {
    models.user.findOne({
        where: {
            username: req.params.name
        }
    })
    .then(user => {
        return models.product.findAll({
            where: {
                userId: user.id
            }
        })
    })
    .then(result => res.send(result))
    .catch(err => res.send(err))
})

router.get('/:id', (req, res) => {
    models.product.findOne({
        where: {
            id: {
                [Op.eq]: req.params.id
            }
        },
        include: [
            { model: models.user }
        ]
    })
        .then(result => res.send(result))
        .catch(err => res.send(err))
})

router.post('/', (req, res) => {
    let data = req.body
    data.createdAt = new Date()
    data.updatedAt = new Date()
    models.product.create(data)
        .then(result => res.sendStatus(201))
        .catch(err => res.send(err))
})

router.put('/:id', (req, res) => {
    let data = req.body
    data.updatedAt = new Date()
    models.product.findById(req.params.id)
        .then(product => {
            product.update(data)
                .then(result => res.send(200))
                .catch(err => res.send(err))
        }).catch(err => res.send(err))
})

module.exports = router