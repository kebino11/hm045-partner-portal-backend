const express = require('express')
const router = express.Router()
const models = require('../models')
const Op = require('sequelize').Op

router.get('/', (req, res) => {
    models.transaction.findAll({
        include: [
            { model: models.user },
            { model: models.product },
            { model: models.transactionType}
        ]
    })
    .then(result => res.send(result))
    .catch(err => res.send(err))
})

router.get('/:id', (req, res) => {
    models.transaction.findOne({
        where: {
            id: {
                [Op.eq]: req.params.id
            }
        },
        include: [
            { model: models.user },
            { model: models.product },
            { model: models.transactionType}
        ]
    })
        .then(result => res.send(result))
        .catch(err => res.send(err))
})

router.post('/', (req, res) => {
    let data = req.body
    data.createdAt = new Date()
    data.updatedAt = new Date()
    models.transaction.create(data)
        .then(result => res.sendStatus(201))
        .catch(err => res.send(err))
})

router.put('/:id', (req, res) => {
    let data = req.body
    data.updatedAt = new Date()
    models.transaction.findById(req.params.id)
        .then(transaction => {
            transaction.update(data)
                .then(result => res.send(200))
                .catch(err => res.send(err))
        }).catch(err => res.send(err))
})

module.exports = router