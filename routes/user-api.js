const express = require('express')
const router = express.Router()
const models = require('../models')
const Op = require('sequelize').Op

router.get('/', (req, res) => {
    models.user.findAll({})
        .then(result => res.send(result))
        .catch(err => res.send(err))
})

router.get('/:id', (req, res) => {
    models.user.findById(req.params.id)
        .then(result => res.send(result))
        .catch(err => res.send(err))
})

router.get('/:id/transaction', (req, res) => {
    models.user.findById(req.params.id)
        .then(user => {
            return models.transaction.findAll({
                where: {
                    userId: user.id
                }
            })
        })
        .then(transactions => {
            let data = transactions.reduce((prev, curr) => {
                return prev + parseFloat(curr.amount)
            }, 0.00)

            res.send({ total: data })
        })
        .catch(err => res.send(err))
})

router.post('/', (req, res) => {
    let data = req.body
    data.createdAt = new Date()
    data.updatedAt = new Date()
    models.user.create(data)
        .then(result => res.sendStatus(201))
        .catch(err => res.send(err))
})

router.put('/:id', (req, res) => {
    let data = req.body
    data.updatedAt = new Date()
    models.user.findById(req.params.id)
        .then(user => {
            user.update(data)
                .then(result => res.send(200))
                .catch(err => res.send(err))
        }).catch(err => res.send(err))
})

module.exports = router